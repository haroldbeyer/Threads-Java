package br.ucsal.pilhas;

public class ProducerPilha implements Runnable {

	private Pilha pilha;
	private final char[] ALFABETO = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
			'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	
	ProducerPilha (Pilha pilha) {
		this.pilha = pilha;
	}
	@Override
	public void run() {
		for (int i = 0; i < ALFABETO.length; i++) {
			pilha.push(ALFABETO[i]);
		}
		System.out.println("PRODUCER DONE!");
		
	}

	
}
