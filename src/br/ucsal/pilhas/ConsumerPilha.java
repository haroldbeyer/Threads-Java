package br.ucsal.pilhas;

public class ConsumerPilha implements Runnable {

	private Pilha pilha;
	private static final int TENTATIVAS = 3;
	
	ConsumerPilha (Pilha pilha) {
		this.pilha = pilha;
	}
	@Override
	public void run() {
		for (int i = 0; i < TENTATIVAS; i++) {
			System.out.println("CONSUMIDOR" + pilha.pop());
		}
		System.out.println("CONSUMER DONE!");
//		System.out.println();
		
	}
}
